Rails.application.routes.draw do
  resources :produtor do
    resources :transacao
    resources :produtos
    get :vizinhos
    collection do
      post :vizinhos_2
      post :login
    end
  end
  post 'produtos_proximos', to: 'produto#produtos_proximos'
end
