FactoryGirl.define do
  factory :Transacao do
    nome { Faker::Lorem.word }
    senha { Faker::Hacker.adjective }
    cpf_cnpj { Faker::Code.ean }
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
    created_at { Faker::Number.number(10) }
  end
end
