FactoryGirl.define do
  factory :Produto do |f|
    f.desc { Faker::Lorem.word }
    f.preco_kg { Faker::Number.decimal(2) }
    f.peso_min { Faker::Number.decimal(2) }
    f.peso_max { Faker::Number.decimal(2) }
    f.especie { Faker::Business.credit_card_type }
    f.produtor
  end
end
