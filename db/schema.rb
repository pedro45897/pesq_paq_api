# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171028012756) do

  create_table "oferta", force: :cascade do |t|
    t.integer "produto_id"
    t.integer "quant"
    t.float "preco_acor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "produtors", force: :cascade do |t|
    t.string "nome"
    t.string "cpf_cnpj"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "latitude"
    t.float "longitude"
    t.string "senha"
    t.string "email"
    t.integer "tipo"
    t.float "nota"
    t.integer "numero_calotes"
  end

  create_table "produtos", force: :cascade do |t|
    t.text "desc"
    t.float "preco_kg"
    t.float "peso_min"
    t.float "peso_max"
    t.string "especie"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "produtor_id"
  end

  create_table "transacaos", force: :cascade do |t|
    t.string "cpf_prod"
    t.string "cpf_distr"
    t.integer "oferta_id"
    t.date "data_compra"
    t.string "modalidade_pag"
    t.date "data_pag"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
