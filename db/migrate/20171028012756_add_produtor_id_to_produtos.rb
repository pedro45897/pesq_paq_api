class AddProdutorIdToProdutos < ActiveRecord::Migration[5.1]
  def change
    add_column :produtos, :produtor_id, :integer
  end
end
