class CreateProdutos < ActiveRecord::Migration[5.1]
  def change
    create_table :produtos do |t|
      t.text :desc
      t.float :preco_kg
      t.float :peso_min
      t.float :peso_max
      t.string :especie

      t.timestamps
    end
  end
end
