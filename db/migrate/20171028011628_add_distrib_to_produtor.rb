class AddDistribToProdutor < ActiveRecord::Migration[5.1]
  def change
    add_column :produtors, :tipo, :integer
    add_column :produtors, :nota, :float
    add_column :produtors, :numero_calotes, :integer
    drop_table :distribuidors
  end
end
