class AddEmailSenhaContatoToProdutor < ActiveRecord::Migration[5.1]
  def change
    add_column :produtors, :senha, :string
    add_column :produtors, :email, :string
  end
end
