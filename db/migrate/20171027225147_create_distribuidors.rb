class CreateDistribuidors < ActiveRecord::Migration[5.1]
  def change
    create_table :distribuidors do |t|
      t.string :nome
      t.string :cpf_cnpj
      t.float :lat
      t.float :longi
      t.float :nota
      t.integer :numero_calote

      t.timestamps
    end
  end
end
