class CreateProdutors < ActiveRecord::Migration[5.1]
  def change
    create_table :produtors do |t|
      t.string :nome
      t.string :cpf_cnpj
      t.float :lat
      t.float :longi

      t.timestamps
    end
  end
end
