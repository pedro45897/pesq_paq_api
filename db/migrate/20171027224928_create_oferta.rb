class CreateOferta < ActiveRecord::Migration[5.1]
  def change
    create_table :oferta do |t|
      t.integer :produto_id
      t.integer :quant
      t.float :preco_acor

      t.timestamps
    end
  end
end
