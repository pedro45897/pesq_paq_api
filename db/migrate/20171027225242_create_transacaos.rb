class CreateTransacaos < ActiveRecord::Migration[5.1]
  def change
    create_table :transacaos do |t|
      t.string :cpf_prod
      t.string :cpf_distr
      t.integer :oferta_id
      t.date :data_compra
      t.string :modalidade_pag
      t.date :data_pag
      t.string :status

      t.timestamps
    end
  end
end
