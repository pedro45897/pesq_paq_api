class AddLatitudeAndLongitudeToModel < ActiveRecord::Migration[5.1]
  def change
    add_column :produtors, :latitude, :float
    add_column :produtors, :longitude, :float
    remove_column :produtors, :lat
    remove_column :produtors, :longi
  end
end
