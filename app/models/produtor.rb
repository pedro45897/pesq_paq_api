class Produtor < ApplicationRecord
  geocoded_by :address
  after_validation :reverse_geocode

  has_many :produtos
end
