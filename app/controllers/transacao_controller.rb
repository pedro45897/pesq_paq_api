class TransacaoController < ApplicationController
  def index
    @produtor = Produtor.find(param[:produtor_id])
    @transacoes = @produtor.transacaos
    json_response(@transacoes)
  end
end
