class ProdutorController < ApplicationController
  def index
    @produtores = Produtor.all
    json_response(@produtores)
  end

  def create
    @produtor = Produtor.new(produtor_params)
    if @produtor.save
      json_response(@produtor)
    end
  end

  def destroy
    @produtor = Produtor.find(params[:id])
    @produtor.destroy
    msg = { status: "deletado", messagem: @produtor.nome + " deletado" }
    render json: msg
  end

  def vizinhos
    @produtor = Produtor.find(params[:produtor_id])
    @vizinhos = @produtor.nearbys
    json_response(@vizinhos)
  end

  def vizinhos_2
    @vizinhos = Produtor.near([params[:latitude],params[:longitude]],params[:distancia],units: :km)
    json_response(@vizinhos)
  end

  def login
    @produtor = Produtor.find_by nome: params[:nome]
    if @produtor
      if @produtor.senha == params[:senha]
        json_response(@produtor)
      else
        msg = { status: "Senha incorreta" }
        render json: msg
      end
    else
      msg = { status: "Usuario nao encontrado" }
      render json: msg
    end
  end

  private

  def produtor_params
    params.require(:produtor).permit(:nome,:cpf_cnpj,:latitude,:longitude,:senha,:email,:tipo,:nota,:numero_calotes)
  end
end
