class ProdutoController < ApplicationController
  def produtos_proximos
    @produtor_vizinhos = Produtor.near([params[:latitude],params[:longitude]],params[:distancia],units: :km)
    @produtos_vizinhos = Array.new
    @produtor_vizinhos.each do |produtor|
      @produtos_vizinhos << produtor.produtos
    end
    json_response(@produtos_vizinhos)
  end

  def index
    @produtor = Produtor.find(params[:produtor_id])
    @produtos = @produtor.produtos
    json_response(@produtos)
  end

  def create
    @produtor = Produtor.find(params[:produtor_id])
    @produto = @produtor.produtos.build(produto_params)
    if @produto.save
      json_response(@produto)
    else
      msg = { status: "Produto não foi criado com sucesso" }
      render json: msg
    end
  end

  private

  def produto_params
    params.require(:produto).permit(:desc,:preco_kg,:peso_min,:peso_max,:especie)
  end
end
